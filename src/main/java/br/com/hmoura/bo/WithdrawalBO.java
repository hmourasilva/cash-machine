package br.com.hmoura.bo;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import br.com.hmoura.model.entity.AtmMoneyEntity;
import lombok.Data;
import lombok.extern.log4j.Log4j2;

/**
 * @author HMOURA
 *
 */

@Component
@Data
@Log4j2
public class WithdrawalBO {

	private Long[] moneyNotes;
	private String result;
	private List<AtmMoneyEntity> atmNotes;

	public void withdrawal(Long value, List<AtmMoneyEntity> atmNotes) throws Exception {
		this.atmNotes = atmNotes;
		
		this.loadMoneyNotes(this.listAvailableNotes(atmNotes));
		this.withdrawal(value, this.calculate(value));
	}
	
	/**
	 * @param moneyNotes
	 */
	private void loadMoneyNotes(Long[] moneyNotes) {
		Arrays.sort(moneyNotes);

		long aux;
		for (int i = 0; i < moneyNotes.length / 2; i++) {
			aux = moneyNotes[i];
			moneyNotes[i] = moneyNotes[moneyNotes.length - i - 1];
			moneyNotes[moneyNotes.length - i - 1] = aux;
		}
		this.moneyNotes = moneyNotes;
	}

	/**
	 * @param value
	 * @return
	 * @throws Exception
	 */
	private long[] calculate(long value) throws Exception {
		if (value < 0) {
			throw new Exception("Não é possível sacar um valor negativo.");
		}

		long[] result = new long[moneyNotes.length];
		for (int i = 0; i < moneyNotes.length; i++) {
			result[i] = value / moneyNotes[i];
			value = value % moneyNotes[i];
		}

		if (value != 0) {
			throw new Exception("Não é possível efetuar o saque deste valor.");
		}
		return result;
	}

	/**
	 * @param value
	 * @return
	 * @throws Exception
	 */
	private void withdrawal(Long value, long[] notes) throws Exception {
		if (value == 0) {
			this.result = "Nenhuma nota será entregue.";
		}

		StringBuilder sb = new StringBuilder();
		sb.append("Entregar ");

		int initialLength = sb.length();
		int last = -1;

		for (int i = 0; i < notes.length; i++) {
			if (notes[i] > 0) {
				last = sb.length();
				sb.append(notes[i]).append(" nota").append(notes[i] == 1 ? " de " : "s de ").append("R$")
						.append(moneyNotes[i]).append(",00, ");
				
				this.updateAvailableNotes(moneyNotes[i], notes[i]);
			}
		}

		if (last > initialLength) {
			sb.replace(last - 2, last, " e ");
		}

		sb.replace(sb.length() - 2, sb.length(), ".");
		this.result = sb.toString();
	}

	
	/**
	 * @param note
	 * @param qtd
	 */
	private void updateAvailableNotes(long note, long qtd) {
		log.info("note: " + note + "qtd: " + qtd);
		for (AtmMoneyEntity atmMoneyEntity : atmNotes) {
			if(atmMoneyEntity.getValue() == note) {
				atmMoneyEntity.setQuantity(atmMoneyEntity.getQuantity() - qtd);
			}
		}
	}
	
	/**
	 * @param atmNotes
	 * @return
	 */
	private Long[] listAvailableNotes(List<AtmMoneyEntity> atmNotes) {
		Long[] arrayNotes = new Long[atmNotes.size()];

		try {
			List<Long> notes = atmNotes.stream()
					.map(AtmMoneyEntity::getValue)
					.collect(Collectors.toList());

			arrayNotes = notes.toArray(arrayNotes);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return arrayNotes;
	}
}
