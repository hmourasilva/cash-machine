package br.com.hmoura.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.hmoura.model.entity.AtmEntity;
import br.com.hmoura.model.repository.AtmRepository;

@Service
public class AtmService {

	@Autowired
	private AtmRepository atmRepo;

	public List<AtmEntity> findAll() {
		return this.atmRepo.findAll();
	}
	
	public AtmEntity findByAlias(String alias) {
		return this.atmRepo.findByAlias(alias);
	}

	public AtmEntity save(AtmEntity atm) {
		return this.atmRepo.save(atm);
	}
}
