package br.com.hmoura.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.hmoura.model.entity.AtmMoneyEntity;
import br.com.hmoura.model.repository.AtmNoteRepository;

@Service
public class AtmNoteService {

	@Autowired
	private AtmNoteRepository atmNoteRepo;
	
	public AtmMoneyEntity save(AtmMoneyEntity entity) {
		return this.atmNoteRepo.save(entity);
	}
	
	public void saveAll(List<AtmMoneyEntity> atmNotes) {
		if(atmNotes != null) {
			this.atmNoteRepo.saveAll(atmNotes);	
		}	
	}
	
	public Optional<AtmMoneyEntity>  findById(Long id) {
		return this.atmNoteRepo.findById(id);
	}
}
