package br.com.hmoura.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.hmoura.bo.WithdrawalBO;
import br.com.hmoura.model.dto.WithdrawalDTO;
import br.com.hmoura.model.entity.AtmEntity;
import br.com.hmoura.model.entity.AtmMoneyEntity;

/**
 * @author HMOURA
 *
 */


@Service
public class WithdrawalService {

	@Autowired
	private AtmService atmService;

	@Autowired
	private WithdrawalBO bo;

	/**
	 * @param dto
	 * @return
	 * @throws Exception
	 */
	public WithdrawalDTO execute(WithdrawalDTO dto) throws Exception {

		AtmEntity atm = this.atmService.findByAlias(dto.getAtmAlias());

		// calculando saque
		// resultado encapsulado no objeto BO
		bo.withdrawal(dto.getValue(), atm.getAtmNotes());

		// obtendo resultado
		dto.setResult(bo.getResult());

		// atualizando saldo da atm
		this.balanceUpdate(atm, dto.getValue());

		// atualizando qts de notas disponiveis
		this.notesUpdate(atm, bo.getAtmNotes());

		return dto;
	}

	private void balanceUpdate(AtmEntity atm, Long value) {
		atm.setBalanceAvailable(atm.getBalanceAvailable().subtract(new BigDecimal(value)));
		this.atmService.save(atm);
	}

	private void notesUpdate(AtmEntity atm, List<AtmMoneyEntity> atmNotes) {
		atm.setAtmNotes(atmNotes);
		this.atmService.save(atm);
	}
}
