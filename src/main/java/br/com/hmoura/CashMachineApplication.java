package br.com.hmoura;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import br.com.hmoura.model.entity.AtmEntity;
import br.com.hmoura.model.entity.AtmMoneyEntity;
import br.com.hmoura.service.AtmService;
import br.com.hmoura.service.AtmNoteService;

@SpringBootApplication
@ComponentScan
public class CashMachineApplication implements ApplicationRunner {
	
	@Autowired
	private AtmService cashMachineService;
	
	@Autowired
	private AtmNoteService moneyService;
	
	public static void main(String[] args) {
		SpringApplication.run(CashMachineApplication.class, args);
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
		
		AtmEntity cashEntity = new AtmEntity("ATM_TESTE", new BigDecimal(0));
		cashEntity = this.cashMachineService.save(cashEntity);
		
		Long balance = 0L;
		AtmMoneyEntity note = new AtmMoneyEntity();
		
		long[] valoresCedulas = new long[] { 10L, 20L, 50L, 100L };
		for (int i = 0; i < valoresCedulas.length; i++) {
			note = new AtmMoneyEntity(valoresCedulas[i], 100L, cashEntity);
			balance = (note.getValue() * note.getQuantity());
			
			this.moneyService.save(note);
		}
		
		cashEntity.setBalanceAvailable(new BigDecimal(balance));
		this.cashMachineService.save(cashEntity);
	}
}
