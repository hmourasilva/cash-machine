package br.com.hmoura.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.hmoura.model.entity.AtmMoneyEntity;

@Repository
public interface AtmNoteRepository extends JpaRepository<AtmMoneyEntity, Long> {

}
