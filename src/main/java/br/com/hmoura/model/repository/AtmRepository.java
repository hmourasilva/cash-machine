package br.com.hmoura.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.hmoura.model.entity.AtmEntity;

@Repository
public interface AtmRepository extends JpaRepository<AtmEntity, Long> {

	public AtmEntity findByAlias(String alias);
}
