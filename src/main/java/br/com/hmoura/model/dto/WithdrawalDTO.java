package br.com.hmoura.model.dto;

import lombok.Data;

@Data
public class WithdrawalDTO {

	private String atmAlias;

	private Long value;

	private String result;

	public WithdrawalDTO(String atmAlias, Long value, String result) {
		super();
		this.atmAlias = atmAlias;
		this.value = value;
		this.result = result;
	}

	public WithdrawalDTO() {
		super();
	}
}
