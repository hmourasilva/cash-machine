package br.com.hmoura.model.dto;

import java.math.BigDecimal;
import java.util.List;

import lombok.Data;

@Data
public class AtmDTO {

	private Long id;

	private String alias;
	
	private BigDecimal balanceAvailable;

	private List<MoneyNoteDTO> moneyNotes;
}
