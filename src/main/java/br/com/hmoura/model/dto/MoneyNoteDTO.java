package br.com.hmoura.model.dto;

import lombok.Data;

@Data
public class MoneyNoteDTO {

	private Long id;

	private Long value;

	private Long quantity;
}
