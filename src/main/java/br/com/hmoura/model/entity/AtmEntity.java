package br.com.hmoura.model.entity;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.Data;

@Entity
@Table(name = "ATM")
@Data
@JsonIdentityInfo(
        generator = ObjectIdGenerators.StringIdGenerator.class,
        property="atm")
public class AtmEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="ALIAS")
	private String alias;
	
	@Column(name="BALANCE_AVAILABLE")
	private BigDecimal balanceAvailable;
	
	@OneToMany(mappedBy="atm", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JsonManagedReference
	private List<AtmMoneyEntity> atmNotes;
	
	@OneToMany(mappedBy="atm", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JsonManagedReference
	private List<SupplyHistoryEntity> supplyHistory;

	public AtmEntity(String alias, BigDecimal balanceAvailable) {
		super();
		this.alias = alias;
		this.balanceAvailable = balanceAvailable;
	}

	public AtmEntity() {
		super();
	}
}
