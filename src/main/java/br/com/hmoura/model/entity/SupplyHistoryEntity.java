package br.com.hmoura.model.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.Data;

@Entity
@Table(name = "SUPPLY_HISTORY")
@Data
@JsonIdentityInfo(
        generator = ObjectIdGenerators.StringIdGenerator.class,
        property="id")
public class SupplyHistoryEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name="QUANTITY")
	private Long quantity;
	
	@Column(name="REQUEST_DATE")
	@Temporal(TemporalType.DATE)
	private Date requestDate;
	
	@Column(name="DELIVERY_DATE")
	@Temporal(TemporalType.DATE)
	private Date deliveryDate;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "atm_id", nullable = false)
	private AtmEntity atm;
}
