package br.com.hmoura.model.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "ATM_MONEY")
@Data
public class AtmMoneyEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="VALUE")
	private Long value;
	
	@Column(name="QUANTITY")
	private Long quantity;
	
	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "atm_id", nullable = false)
	private AtmEntity atm;
	
	public AtmMoneyEntity(Long value, Long quantity, AtmEntity atm) {
		super();
		this.value = value;
		this.quantity = quantity;
		this.atm = atm;
	}

	public AtmMoneyEntity() {
		super();
	}
}
