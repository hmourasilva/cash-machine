package br.com.hmoura.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.hmoura.model.entity.AtmEntity;
import br.com.hmoura.service.AtmService;
import io.swagger.annotations.ApiOperation;

/**
 * @author HMOURA
 *
 */


@RestController
@RequestMapping(value = "/v1/atm")
public class CashMachineController {

	@Autowired
	private AtmService cashService;
	
	@GetMapping(produces = APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Retorna lista com todos os caixas eletronicos cadastrados", response = AtmEntity.class) 
	public ResponseEntity<?> findAll(){
		return new ResponseEntity<>(this.cashService.findAll(), HttpStatus.ACCEPTED);
	}
}
