package br.com.hmoura.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.hmoura.model.dto.WithdrawalDTO;
import br.com.hmoura.service.WithdrawalService;
import io.swagger.annotations.ApiOperation;

/**
 * @author HMOURA
 *
 */


@RestController
@RequestMapping(value = "/v1/withdrawal")
public class WithdrawalController {

	@Autowired
	private WithdrawalService service;

	@PutMapping(produces = APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Efetua saque do valor informado", response = WithdrawalDTO.class)
	public ResponseEntity<?> withdrawal(@RequestBody WithdrawalDTO dto) throws Exception {
		return new ResponseEntity<>(this.service.execute(dto), HttpStatus.ACCEPTED);
	}
}
