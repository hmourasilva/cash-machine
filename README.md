# SIMULADOR DE ATM
> Simulador de caixa eletrônico.

Serviço REST que simula o funcionamento basico de um caixa eletrônico.


## Exemplo de uso

Baixe o projeto e execute os passos abaixo:

* 1 - Execute o comando maven no diretorio raiz do projeto

```sh
mvn clean instal
```

* 2 - Execute o comando abaixo no diretorio target do projeto'

```sh
java -jar cash-machine-0.0.1.jar
```

* 3 - O serviço REST será exposto na porta 8080

```sh
http://localhost:8080/swagger-ui.html
```

## Detalhes

Para executar o serviço ATM basta efetuar requisição GET para o endpoint abaixo
```sh
http://localhost:8080/v1/atm
```

Este serviço retorna o status das ATms cadastradas(salto disponivel, valores e 
qtd das notas disponiveis)

Abaixo está um exemplo de response do serviço


```sh
[
    {
        "atm": "4f130a36-8d3f-42ca-91a9-8ef9c91a2893",
        "id": 1,
        "alias": "ATM_TESTE",
        "balanceAvailable": 9920.00,
        "atmNotes": [
            {
                "id": 1,
                "value": 10,
                "quantity": 99,
                "atm": "4f130a36-8d3f-42ca-91a9-8ef9c91a2893"
            },
            {
                "id": 2,
                "value": 20,
                "quantity": 99,
                "atm": "4f130a36-8d3f-42ca-91a9-8ef9c91a2893"
            },
            {
                "id": 3,
                "value": 50,
                "quantity": 99,
                "atm": "4f130a36-8d3f-42ca-91a9-8ef9c91a2893"
            },
            {
                "id": 4,
                "value": 100,
                "quantity": 100,
                "atm": "4f130a36-8d3f-42ca-91a9-8ef9c91a2893"
            }
        ],
        "supplyHistory": []
    }
]
```

Para executar o serviço "withdrawal" basta efetuar requisição PUT para o endpoint abaixo.
Este serviço simula da operacao de saque efetuando o calculo de notas retornadas, subtraindo 
o salgo total disponivel na ATM e subtraindo a qtd de notas utilizadas.

```sh
http://localhost:8080/v1/withdrawal
```

Abaixo está um exemplo de request e response do serviço.

```sh
{
	"atmAlias": "ATM_TESTE",
	"value": 30
}
```

```sh
{
    "atmAlias": "ATM_TESTE",
    "value": 30,
    "result": "Entregar 1 nota de R$20,00 e 1 nota de R$10,00."
}
```




SWAGGER: http://localhost:8080/swagger-ui.html

